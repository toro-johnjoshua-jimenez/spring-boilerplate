package io.toro.ojtbe.jimenez.Simplifly.seeders;

import io.toro.ojtbe.jimenez.Simplifly.models.Model;
import io.toro.ojtbe.jimenez.Simplifly.models.repositories.ModelRepository;
import io.toro.ojtbe.jimenez.Simplifly.models.Test;
import io.toro.ojtbe.jimenez.Simplifly.models.repositories.TestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class H2Seeder implements CommandLineRunner {

    private final Logger logger;

    @Autowired
    private final ModelRepository repo;

    @Autowired
    private final TestRepository testRepo;

    public H2Seeder(ModelRepository repo, TestRepository testRepo){
        this.repo = repo;
        this.testRepo = testRepo;
        this.logger = LoggerFactory.getLogger(H2Seeder.class);
    }

    @Override
    public void run(String... args){
        logger.info("Starting seeding process . . .");

        Model newModel = new Model();
        newModel.setName("hha");
        newModel.setId("haha");

        Model secondModel = new Model();
        secondModel.setName("hha");
        secondModel.setId("DUDUDUDUD");

        repo.save(newModel);
        repo.save(secondModel);

        Test newTest = new Test();
        newTest.setName("sd");

        Test secondTest = new Test();
        secondTest.setName("sddd");

        testRepo.save(newTest);
        testRepo.save(secondTest);

        logger.info("Database seeded!");
    }
}
