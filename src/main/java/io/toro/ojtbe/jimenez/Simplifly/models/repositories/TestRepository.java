package io.toro.ojtbe.jimenez.Simplifly.models.repositories;

import io.toro.ojtbe.jimenez.Simplifly.models.Test;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestRepository extends JpaRepository<Test, Long> {
}
