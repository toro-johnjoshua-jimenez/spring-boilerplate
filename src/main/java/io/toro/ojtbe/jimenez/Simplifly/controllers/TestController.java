package io.toro.ojtbe.jimenez.Simplifly.controllers;

import io.toro.ojtbe.jimenez.Simplifly.models.Test;
import io.toro.ojtbe.jimenez.Simplifly.models.services.TestServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/test")
@RestController
public class TestController extends Controller<Test, Long> {

    @Autowired
    TestController(TestServiceImpl service){
        super(service, Test.class);
    }
}
