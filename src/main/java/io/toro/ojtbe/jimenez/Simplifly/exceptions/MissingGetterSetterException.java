package io.toro.ojtbe.jimenez.Simplifly.exceptions;

public class MissingGetterSetterException extends RuntimeException {
    public MissingGetterSetterException(){
        super();
    }

    public MissingGetterSetterException(String message){
        super(message);
    }

    public MissingGetterSetterException(Throwable cause){
        super(cause);
    }

    public MissingGetterSetterException(String message, Throwable cause){
        super(message, cause);
    }
}
