package io.toro.ojtbe.jimenez.Simplifly.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SeedWith {
    public String type() default "" ;
    public int count() default 1 ;
}
