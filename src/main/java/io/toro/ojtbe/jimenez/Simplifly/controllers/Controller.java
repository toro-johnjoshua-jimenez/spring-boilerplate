package io.toro.ojtbe.jimenez.Simplifly.controllers;

import io.toro.ojtbe.jimenez.Simplifly.exceptions.MissingGetterSetterException;
import io.toro.ojtbe.jimenez.Simplifly.models.services.ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Abstract class that may serve as boilerplate code for your Spring Boot controllers.
 *
 * @param <V> the entity class
 * @param <U> wrapper class for the entity's id
 *
 * @author Joshua Jimenez
 */
abstract class Controller<V, U> {

    @Autowired
    private final ServiceImpl<V, U> service;

    private final Class<V> entityClass;

    private final Logger logger;

    private final Map<String, Class<?>> entityProperties;

    Controller(ServiceImpl<V, U> service, Class<V> entityClass){
        this.service = service;
        this.entityClass = entityClass;
        this.logger = LoggerFactory.getLogger(this.getClass());
        this.entityProperties = getEntityProperties();
    }

    private Map<String, Class<?>> getEntityProperties(){
        Method[] methods = entityClass.getMethods();
        Map<String, Class<?>> properties = new HashMap<>();

        for(Method method: methods){
            String prefix = method.getName().substring(0,3);
            String propertyName = method.getName().substring(3);

            if(prefix.equals("set") && !propertyName.equals("Class")){
                properties.put(propertyName, method.getParameterTypes()[0]);
            }
        }

        return properties;
    }

    @GetMapping
    List<V> getAll(HttpServletRequest request){
        logRequestDetails(request);

        return service.getAllInstances();
    }

    @GetMapping("/{id}")
    ResponseEntity<Object> getOneById(@PathVariable U id, HttpServletRequest request){
        logRequestDetails(request);

        Optional<V> matchedInstance = service.getInstanceById(id);
        if(matchedInstance.isPresent()){
            return new ResponseEntity<>(matchedInstance.get(), HttpStatus.OK);
        } else{
            String message = "No entity with matching id";
            logger.error(message);

            throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
        }
    }

    @PostMapping
    ResponseEntity<Object> saveOne(@RequestBody V newEntity, HttpServletRequest request){
        logRequestDetails(request);

        return new ResponseEntity<>(service.saveEntity(newEntity), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    ResponseEntity<Object> saveOrReplace(@RequestBody V newEntity, @PathVariable U id, HttpServletRequest request){
        logRequestDetails(request);

        Optional<V> matchedEntity = service.getInstanceById(id);
        if(matchedEntity.isPresent()){
            updateEntityProperties(matchedEntity.get(), newEntity);

            return new ResponseEntity<>(service.saveEntity(matchedEntity.get()), HttpStatus.OK);

        } else {
            try{
                entityClass.getMethod("setId").invoke(newEntity, id);

            } catch(NoSuchMethodException e){
                String errorMessage = "No set method for Id";
                logger.error(errorMessage);
                throw new RuntimeException(errorMessage);

            } catch(IllegalAccessException | InvocationTargetException e){
                String errorMessage = "Access violation";
                logger.error(errorMessage);
                throw new RuntimeException(errorMessage);
            }

            return new ResponseEntity<>(service.saveEntity(newEntity), HttpStatus.OK);
        }
    }

    @DeleteMapping("/{id}")
    void deleteEntity(@PathVariable U id, HttpServletRequest request){
        logRequestDetails(request);
        service.deleteIfExists(id);
    }

    private void updateEntityProperties(V matchedEntity, V newEntity){
        for(String property : entityProperties.keySet()){
            try{
                if(!property.equals("Id")){
                    Method setMethod = entityClass.getMethod("set" + property, entityProperties.get(property));
                    Method getMethod = entityClass.getMethod("get" + property);
                    setMethod.invoke(matchedEntity, getMethod.invoke(newEntity));
                }
            } catch(NoSuchMethodException e){
                String errorMessage = "No get or set property for: " + property;
                logger.error(errorMessage);
                throw new MissingGetterSetterException(errorMessage);

            } catch(IllegalAccessException e){
                String errorMessage = "Access violation: " + property;
                logger.error(errorMessage);
                throw new RuntimeException(errorMessage);

            } catch(InvocationTargetException e){
                logger.error("Runtime exceptions at the reflected get set method: " + property);
                throw new RuntimeException(e.getMessage());
            }
        }
    }

    private void logRequestDetails(HttpServletRequest request){
        logger.info("Request received . . .");
        logger.info("URI: " + request.getRequestURI());
        logger.info("Method: " + request.getMethod());
        logger.info("Queries: "+ request.getQueryString());
        logger.info("Content-Type: " + request.getContentType());
        logger.info("Remote Ip Addr: " + request.getRemoteAddr());
    }
}
