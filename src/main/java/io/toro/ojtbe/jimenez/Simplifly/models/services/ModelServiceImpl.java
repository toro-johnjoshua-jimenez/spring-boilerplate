package io.toro.ojtbe.jimenez.Simplifly.models.services;

import io.toro.ojtbe.jimenez.Simplifly.models.Model;
import io.toro.ojtbe.jimenez.Simplifly.models.repositories.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Transactional
@Service
public class ModelServiceImpl extends ServiceImpl<Model, String> {

    @Autowired
    public ModelServiceImpl(ModelRepository repository){
        super(repository);
    }
}