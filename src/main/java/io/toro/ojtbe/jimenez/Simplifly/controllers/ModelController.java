package io.toro.ojtbe.jimenez.Simplifly.controllers;

import io.toro.ojtbe.jimenez.Simplifly.models.Model;
import io.toro.ojtbe.jimenez.Simplifly.models.services.ModelServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/dogs")
@RestController
public class ModelController extends Controller<Model, String> {

    @Autowired
    public ModelController(ModelServiceImpl modelService){
        super(modelService, Model.class);
    }
}
