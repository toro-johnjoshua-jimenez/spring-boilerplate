package io.toro.ojtbe.jimenez.Simplifly.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
public class Model {
    private @Id String id;
    private String name;
}
