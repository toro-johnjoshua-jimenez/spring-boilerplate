package io.toro.ojtbe.jimenez.Simplifly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpliFlyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpliFlyApplication.class, args);
	}

}

