package io.toro.ojtbe.jimenez.Simplifly.models.repositories;

import io.toro.ojtbe.jimenez.Simplifly.models.Model;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModelRepository extends JpaRepository<Model, String> {
}
