package io.toro.ojtbe.jimenez.Simplifly.models.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Abstract class that may server as boilerplate code for your Spring Boot
 * service implementations.
 *
 * @param <S> the specific entity class
 * @param <V> the object wrapper for the id of the entity
 * @see Service interface
 *
 * @author Joshua Jimenez
 */
public abstract class ServiceImpl<S, V> implements Service<S, V> {

    @Autowired
    private final JpaRepository<S, V> entityRepo;

    private final Logger logger;

    public ServiceImpl(JpaRepository<S, V> entityRepo){
        this.entityRepo = entityRepo;
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    @Override
    public Page<S> getAllInstances(Pageable pageable){
        logger.debug("Call on pageable getAllInstances");
        return entityRepo.findAll(pageable);
    }

    @Override
    public List<S> getAllInstances(){
        logger.debug("Call on getAllInstances");
        return entityRepo.findAll();
    }

    @Override
    public Optional<S> getInstanceById(V id){
        logger.debug("Call on getInstanceById");
        return entityRepo.findById(id);
    }

    @Override
    public Object saveEntity(S newEntity){
        logger.debug("Call on saveEntity");
        return entityRepo.save(newEntity);
    }

    @Override
    public void deleteIfExists(V id){
        logger.debug("Call on deleteIfExists");
        Optional<S> match = entityRepo.findById(id);
        if(match.isPresent()){
            entityRepo.deleteById(id);
        }
    }
}
