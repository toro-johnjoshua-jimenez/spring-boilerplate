package io.toro.ojtbe.jimenez.Simplifly.annotations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public final class AnnotationScanner {

    private final Logger logger;

    public AnnotationScanner(){
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    public List<Class<?>> findAnnotatedClasses(Class<? extends Annotation> annotationClass, String basePackage){
        ClassPathScanningCandidateComponentProvider provider = createComponentScanner(annotationClass);
        List<Class<?>> annotatedClasses = new ArrayList<>();

        for(BeanDefinition bean : provider.findCandidateComponents(basePackage)){
            try{
                Class<?> beanClass = Class.forName(bean.getBeanClassName());
                annotatedClasses.add(beanClass);

            } catch(ClassNotFoundException e){
                logger.error("Class not found.");
                throw new RuntimeException(e.getMessage());
            }
        }

        return annotatedClasses;
    }

    public List<Field> findAnnotatedFields(Class<?> containerClass, Class<? extends Annotation> annotationClass){
        List<Field> annotatedFields = new ArrayList<>();

        for(Field field : containerClass.getFields()){
            if(field.getAnnotation(annotationClass) != null){
                annotatedFields.add(field);
            }
        }

        return annotatedFields;
    }

    private ClassPathScanningCandidateComponentProvider createComponentScanner(Class<? extends Annotation> annotationClass){
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(annotationClass));

        return scanner;
    }
}
