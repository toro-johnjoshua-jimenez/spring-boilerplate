package io.toro.ojtbe.jimenez.Simplifly.annotations;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Method;

@Getter
@Setter
public final class Seed {
    private final Class<?> entityClass;
    private final Method fakerGeneratorMethod;
    private final String propertyName;
    private final Class<?> parameterType;
    private final int count;

    private Seed(Class<?> entityClass, Method fakerGeneratorMethod, String propertyName, Class<?> parameterType, int count) {
        this.entityClass = entityClass;
        this.fakerGeneratorMethod = fakerGeneratorMethod;
        this.propertyName = propertyName;
        this.parameterType = parameterType;
        this.count = count;
    }

    public static class Builder{
        private Class<?> entityClass;
        private Method fakerGeneratorMethod;
        private String propertyName;
        private Class<?> parameterType;
        private int count;

        public Builder addEntityClass(Class<?> entityClass){
            this.entityClass = entityClass;
            return this;
        }

        public Builder addFakerGeneratorMethod(Method fakerGeneratorMethod){
            this.fakerGeneratorMethod = fakerGeneratorMethod;
            return this;
        }

        public Builder addPropertyName(String propertyName){
            this.propertyName = propertyName;
            return this;
        }

        public Builder addParameterType(Class<?> parameterType){
            this.parameterType = parameterType;
            return this;
        }

        public Builder addCount(int count){
            this.count = count;
            return this;
        }

        public Seed build(){
            return new Seed(entityClass, fakerGeneratorMethod, propertyName, parameterType, count);
        }
    }
}
