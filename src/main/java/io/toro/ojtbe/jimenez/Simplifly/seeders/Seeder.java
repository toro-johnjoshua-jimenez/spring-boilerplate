package io.toro.ojtbe.jimenez.Simplifly.seeders;

import io.toro.ojtbe.jimenez.Simplifly.annotations.AnnotationScanner;
import io.toro.ojtbe.jimenez.Simplifly.annotations.Seed;
import io.toro.ojtbe.jimenez.Simplifly.annotations.SeedClass;
import io.toro.ojtbe.jimenez.Simplifly.annotations.SeedWith;
import io.toro.ojtbe.jimenez.Simplifly.exceptions.SeedCountException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@Component
public class Seeder implements CommandLineRunner {

    private final Logger logger;

    @Autowired
    private final AnnotationScanner annotationScanner;

    public Seeder(AnnotationScanner annotationScanner){
        this.annotationScanner = annotationScanner;
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    @Override
    public void run(String... args){

    }

    private void startSeeding(){
        List<Seed> seedDefinitions = getSeedDefinitions();
        Class<?> entityClass = null;
        Object instance = null;

        for(Seed seedDefinition : seedDefinitions){
            int count = seedDefinition.getCount();

            if(count < 0){
                logger.error("Seed count should be greather than zero (0).");
                throw new SeedCountException(seedDefinition.getEntityClass().getSimpleName());
            }

            //Todo: get repositories on annotation
        }
    }

    private List<Seed> getSeedDefinitions(){
        List<Seed> seedDefinitions = new ArrayList<>();
        List<Class<?>> seedClasses = annotationScanner.findAnnotatedClasses(SeedClass.class, "io.toro.ojtbe.jimenez.Simplifly.models");

        for(Class<?> seedClass : seedClasses){
            List<Field> annotatedFields = annotationScanner.findAnnotatedFields(seedClass, SeedWith.class);

            for(Field field : annotatedFields){
                SeedWith values = field.getAnnotation(SeedWith.class);
                Method fakerGeneratorMethod = getMethodEquivalent(seedClass, values.type());
                Seed seed = new Seed.Builder()
                        .addEntityClass(seedClass)
                        .addCount(values.count())
                        .addFakerGeneratorMethod(fakerGeneratorMethod)
                        .addParameterType(field.getType())
                        .addPropertyName(field.getName())
                        .build();
                seedDefinitions.add(seed);
            }
        }

        return seedDefinitions;
    }

    private Method getMethodEquivalent(Class<?> container, String method){
        try{
            return container.getMethod(method);

        } catch(NoSuchMethodException e){
            logger.error(method + " does not exist!");
            throw new RuntimeException(e.getMessage());
        }
    }

    private Object createNewInstance(Class<?> entityClass){
        try{
            return entityClass.newInstance();

        } catch(InstantiationException e){
            logger.error("Failed to create an instance of " + entityClass.getSimpleName());
            logger.error("Entity classes / models should contain a noargs constructor");
            throw new RuntimeException(e.getMessage());

        } catch(IllegalAccessException e){
            logger.error("Cannot access " + entityClass.getSimpleName());
            throw new RuntimeException(e.getMessage());
        }

    }

}
