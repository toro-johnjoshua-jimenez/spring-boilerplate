package io.toro.ojtbe.jimenez.Simplifly.models.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 *
 * @param <T> the specific entity class
 * @param <S> the class wrapper for entity's id
 */
interface Service<T, S> {
    Page<T> getAllInstances(Pageable pageable);
    List<T> getAllInstances();
    Optional<T> getInstanceById(S id);
    Object saveEntity(T newEntity);
    void deleteIfExists(S id);
}
