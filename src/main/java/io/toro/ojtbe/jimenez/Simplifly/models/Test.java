package io.toro.ojtbe.jimenez.Simplifly.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
public class Test {
    private @Id @GeneratedValue Long id;
    private String name;
}
