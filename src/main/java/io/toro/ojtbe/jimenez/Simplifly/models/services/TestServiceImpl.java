package io.toro.ojtbe.jimenez.Simplifly.models.services;

import io.toro.ojtbe.jimenez.Simplifly.models.Test;
import io.toro.ojtbe.jimenez.Simplifly.models.repositories.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class TestServiceImpl extends ServiceImpl<Test, Long> {

    @Autowired
    public TestServiceImpl(TestRepository repo){
        super(repo);
    }
}
